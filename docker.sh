echo 
echo "******************************************"
echo "**  Creating limitbandwidth Container   **"
echo "******************************************"
echo 

docker build -t limitbandwidth -f limitbandwidth.dockerfile .
docker tag limitbandwidth fayssalmekk/limitbandwidth:latest
docker push fayssalmekk/limitbandwidth:latest

echo 
echo "******************************************"
echo "**  Creating resetbandwidth Container   **"
echo "******************************************"
echo 


docker build -t resetbandwidth -f resetbandwidth.dockerfile .
docker tag resetbandwidth fayssalmekk/resetbandwidth:latest
docker push fayssalmekk/resetbandwidth:latest
