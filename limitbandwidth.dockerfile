FROM python:3.9-alpine
RUN pip install requests kubernetes
COPY limitBandwidth.py /app/limitBandwidth.py
CMD ["python", "/app/limitBandwidth.py"]