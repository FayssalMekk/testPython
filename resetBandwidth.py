
import re

from kubernetes import client, config
config.load_incluster_config()
api_client = client.AppsV1Api()

namespace_pattern = r"[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$"
stateful_sets = api_client.list_stateful_set_for_all_namespaces().items


for stateful_set in stateful_sets:
    if re.match(namespace_pattern, stateful_set.metadata.namespace) and 'kubernetes.io/ingress-bandwidth' in stateful_set.spec.template.metadata.annotations:
        del stateful_set.spec.template.metadata.annotations['kubernetes.io/ingress-bandwidth']
        print(f"Deleted ingress-bandwidth annotation from StatefulSet {stateful_set.metadata.name} in namespace {stateful_set.metadata.namespace}")
    if re.match(namespace_pattern, stateful_set.metadata.namespace) and 'kubernetes.io/egress-bandwidth' in stateful_set.spec.template.metadata.annotations:
        del stateful_set.spec.template.metadata.annotations['kubernetes.io/egress-bandwidth']
        print(f"Deleted egress-bandwidth annotation from StatefulSet {stateful_set.metadata.name} in namespace {stateful_set.metadata.namespace}")
    api_client.replace_namespaced_stateful_set(name=stateful_set.metadata.name, namespace=stateful_set.metadata.namespace, body=stateful_set)