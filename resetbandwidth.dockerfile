FROM python:3.9-alpine
RUN pip install requests kubernetes
COPY resetBandwidth.py /app/limitBandwidth.py
CMD ["python", "/app/limitBandwidth.py"]